/*==================================================
 *  Localization of labellers.js
 *==================================================
 */

Timeline.GregorianDateLabeller.monthNames["ca"] = [
    "Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Des"
];

Timeline.GregorianDateLabeller.dayNames["ca"] = [
    "Diumenge", "Diluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"
];