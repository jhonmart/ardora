/*==================================================
 *  Localization of labellers.js
 *==================================================
 */

Timeline.GregorianDateLabeller.monthNames["gz"] = [
    "Xan", "Feb", "Mar", "Abr", "Mai", "Xuñ", "Xul", "Ago", "Set", "Out", "Nov", "Dec"
];

Timeline.GregorianDateLabeller.dayNames["gz"] = [
    "Domingo", "Luns", "Martes", "Mércores", "Xoves", "Venres", "Sábado"
];