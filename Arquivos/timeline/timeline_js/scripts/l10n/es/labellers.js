/*==================================================
 *  Localization of labellers.js
 *==================================================
 */

Timeline.GregorianDateLabeller.monthNames["es"] = [
    "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"
];

Timeline.GregorianDateLabeller.dayNames["es"] = [
    "Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado"
];