/*==================================================
 *  Localization of labellers.js
 *==================================================
 */

Timeline.GregorianDateLabeller.monthNames["eu"] = [
    "Urt", "Ots", "Mar" "Api", "Mai", "Eka" "Uzt", "Abu", "Ira", "Urr", "Aza", "Abe"
];

Timeline.GregorianDateLabeller.dayNames["eu"] = [
    "Igandea", "Astelehena", "Asteartea", "Asteazkena", "Osteguna", "Ostirala", "Larunbata"
];
