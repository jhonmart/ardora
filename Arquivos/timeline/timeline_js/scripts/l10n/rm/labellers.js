/*==================================================
 *  Localization of labellers.js
 *==================================================
 */

Timeline.GregorianDateLabeller.monthNames["rm"] = [
    "Ian", "Feb", "Mar", "Apr", "Poa", "Iun", "Iul", "Aug", "Sep", "Oct", "Noi", "Dec"
];

Timeline.GregorianDateLabeller.dayNames["rm"] = [
    "Duminica", "Luni", "Marti", "Miercuri", "Joi", "Vineri", "S�mbata"
];
