$folderGroup=$_POST["folderG"];
$xml = new DOMDocument('1.0', 'utf-8');
$xml->formatOutput = true;
$xml->preserveWhiteSpace = false;
$xml->load("../".folderWork.$folderGroup."/objects.xml");
if ($_POST["typeObj"]=="0"){
	$back=$xml->getElementsByTagName("obj")->item(0);
	$isOpen=$back->getElementsByTagName("isOpen")->item(0);
	$dtOpen=$back->getElementsByTagName("dtOpen")->item(0);
	$usrOpen=$back->getElementsByTagName("usrOpen")->item(0);
	$isOpen->nodeValue="Y";
	$dtOpen->nodeValue=date("d-m-Y H:i:s");
	$usrOpen->nodeValue=$slogin_Username;
	$xml->save("../".folderWork.$folderGroup."/objects.xml");
}
if ($_POST["typeObj"]=="obj"){
	$nId=(string) $_POST["numId"];
	$nodes=$xml->getElementsByTagName("obj");
	for ($i = 0; $i < $nodes->length; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);
		$num=$back->getElementsByTagName("id")->item(0);
		$numId=(string)$num->nodeValue;
		if ($numId===$nId){
			$isOpen=$back->getElementsByTagName("isOpen")->item(0);
			$dtOpen=$back->getElementsByTagName("dtOpen")->item(0);
			$usrOpen=$back->getElementsByTagName("usrOpen")->item(0);
			$isOpen->nodeValue="Y";
			$dtOpen->nodeValue=date("d-m-Y H:i:s");
			$usrOpen->nodeValue=$slogin_Username;
			$xml->save("../".folderWork.$folderGroup."/objects.xml");		
		}	
	}
	echo json_encode(array("numId"=>$numId, "id"=>$nId));
}
if ($_POST["typeObj"]=="deleteOBJ"){
	$rut="Entrando";
	$nId=(string) $_POST["numId"];
	$nodes=$xml->getElementsByTagName("obj");
	for ($i = 0; $i < $nodes->length; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);
		$num=$back->getElementsByTagName("id")->item(0);
		$type=$back->getElementsByTagName("type")->item(0);
		$innerHtml=$back->getElementsByTagName("innerHtml")->item(0);
		$numId=(string)$num->nodeValue;
		if ($numId===$nId){
			$ruta=(string)$innerHtml->nodeValue;
			if (substr($ruta,0,18)=="../ardoraWorkFiles"){
				if(file_exists("../".$ruta)){
					unlink("../".$ruta);
				}
				if ((string)$type->nodeValue=="Image"){
					$origRuta=str_replace("thumbs","original",$ruta);
					if(file_exists("../".$origRuta)){
						unlink("../".$origRuta);
					}	
				}
			}
			$back->parentNode->removeChild($back);
			$xml->save("../".folderWork.$folderGroup."/objects.xml");		
		}	
	}
	echo json_encode(array("cadea"=>$rut));	
}
?>

