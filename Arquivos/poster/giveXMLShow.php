$xml = new DOMDocument('1.0', 'utf-8');
$xml->formatOutput = true;
$xml->preserveWhiteSpace = false;
if ($_GET["typeObj"]=="getPathShow"){
	$xml->load("../".folderWork."/foldersWork.xml");
	$nId=intval($_GET["num"]);
	$nodes=$xml->getElementsByTagName("folder");
	for ($i = 0; $i < $nodes->length; $i++) {
		$back=$xml->getElementsByTagName("folder")->item($i);
		$num=$back->getElementsByTagName("id")->item(0);
		$numId=intval($num->nodeValue);
		if ($numId==$nId){
			$folderName=$back->getElementsByTagName("folderName")->item(0);
			$title=$back->getElementsByTagName("title")->item(0);
			$folderName=$folderName->nodeValue;
			$title=$title->nodeValue;
			echo json_encode(array("fN"=>$folderName, "tit"=>$title));
		}
	}
}
if ($_GET["typeObj"]=="back"){
	$pathF=$_GET["path"]."/objects.xml";
	$xml->load($pathF);
	$back=$xml->getElementsByTagName("obj")->item(0);
	$cssStyle=$back->getElementsByTagName("css")->item(0);
	echo json_encode(array("cssStyle"=>$cssStyle->nodeValue));
}
if ($_GET["typeObj"]=="objects"){$listId="";$listType="";$listAlt="";$listComent="";$listInnerHtml="";$listLink="";$listCss="";$listDt="";
	$pathF=$_GET["path"]."/objects.xml";
	$xml->load($pathF);
	$num=$xml->getElementsByTagName("obj")->length;
	for ($i=0; $i<$num; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);	
		$id=$back->getElementsByTagName("id")->item(0);
		$type=$back->getElementsByTagName("type")->item(0);
		$alt=$back->getElementsByTagName("alt")->item(0);
		$coment=$back->getElementsByTagName("coment")->item(0);
		$innerHtml=$back->getElementsByTagName("innerHtml")->item(0);
		$link=$back->getElementsByTagName("link")->item(0);
		$cssStyle=$back->getElementsByTagName("css")->item(0);
		$dt=$back->getElementsByTagName("dt")->item(0);
		$listId=$listId.$id->nodeValue."~";
		$listType=$listType.$type->nodeValue."~";
		$listAlt=$listAlt.$alt->nodeValue."~";
		$listComent=$listComent.$coment->nodeValue."~";
		$listInnerHtml=$listInnerHtml.$innerHtml->nodeValue."~";
		$listLink=$listLink.$link->nodeValue."~";
		$listCss=$listCss.$cssStyle->nodeValue."~";
		$listDt=$listDt.$dt->nodeValue."~";	
	}
	echo json_encode(array("listId"=>$listId,"listType"=>$listType,"listAlt"=>$listAlt, "listComent"=>$listComent,"listInnerHtml"=>$listInnerHtml,"listLink"=>$listLink,"listCss"=>$listCss,"listDt"=>$listDt,"num"=>$num));
}
if ($_GET["typeObj"]=="giveMeProperties"){
	$folG="../".folderWork.$_GET["folderG"];
	$xml->load("../".folderWork."/foldersWork.xml");
	$nodes=$xml->getElementsByTagName("folder");
	for ($i = 0; $i < $nodes->length; $i++) {
		$back=$xml->getElementsByTagName("folder")->item($i);
		$fol=$back->getElementsByTagName("folderName")->item(0);
		$folId=(string) $fol->nodeValue;
		if ($folId==$folG){
			$thumb=$back->getElementsByTagName("thumb")->item(0);
			$title=$back->getElementsByTagName("title")->item(0);
			$thumb=$thumb->nodeValue;
			$title=$title->nodeValue;
		}	
	}
	echo json_encode(array("th"=>$thumb, "tit"=>$title, "fol"=>"../".folderWork));
}
if ($_GET["typeObj"]=="giveMeObject"){$listId="";$listType="";$listAlt="";$listComent="";$listInnerHtml="";$listLink="";$listCss="";$listDt="";
	$pathF=$_GET["path"]."/objects.xml";
	$numId=$_GET["numId"];
	$xml->load($pathF);
	$num=$xml->getElementsByTagName("obj")->length;
	for ($i=0; $i<$num; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);	
		$id=$back->getElementsByTagName("id")->item(0);
		if ($id=$numId){
			$type=$back->getElementsByTagName("type")->item(0);
			$alt=$back->getElementsByTagName("alt")->item(0);
			$coment=$back->getElementsByTagName("coment")->item(0);
			$innerHtml=$back->getElementsByTagName("innerHtml")->item(0);
			$link=$back->getElementsByTagName("link")->item(0);
			$cssStyle=$back->getElementsByTagName("css")->item(0);
			$dt=$back->getElementsByTagName("dt")->item(0);
			$listId=$id->nodeValue;
			$listType=$type->nodeValue;
			$listAlt=$alt->nodeValue;
			$listComent=$coment->nodeValue;
			$listInnerHtml=$innerHtml->nodeValue;
			$listLink=$link->nodeValue;
			$listCss=$cssStyle->nodeValue;
			$listDt=$dt->nodeValue."~";
			$nId=$numId;
		}	
	}
	echo json_encode(array("listType"=>$listType,"listAlt"=>$listAlt, "listComent"=>$listComent,"listInnerHtml"=>$listInnerHtml,"listLink"=>$listLink,"listCss"=>$listCss,"listDt"=>$listDt,"num"=>$num,"numId"=>$nId));
}
?>


