$folderGroup=$_POST["folderG"];
$xml = new DOMDocument('1.0', 'utf-8');
	$xml->formatOutput = true;
	$xml->preserveWhiteSpace = false;
	$xml->load("../".folderWork.$folderGroup."/objects.xml");
if ($_POST["typeSave"]=="saveBack"){
	$back=$xml->getElementsByTagName("obj")->item(0);
	$id=$back->getElementsByTagName("id")->item(0);
	$type=$back->getElementsByTagName("type")->item(0);
	$isOpen=$back->getElementsByTagName("isOpen")->item(0);
	$dtOpen=$back->getElementsByTagName("dtOpen")->item(0);
	$usrOpen=$back->getElementsByTagName("usrOpen")->item(0);
	$usr=$back->getElementsByTagName("usr")->item(0);
	$coment=$back->getElementsByTagName("coment")->item(0);
	$alt=$back->getElementsByTagName("alt")->item(0);
	$innerHtml=$back->getElementsByTagName("innerHtml")->item(0);
	$link=$back->getElementsByTagName("link")->item(0);
	$css=$back->getElementsByTagName("css")->item(0);
	$dt=$back->getElementsByTagName("dt")->item(0);
	$id->nodeValue=0;
	$type->nodeValue="back";
	$isOpen->nodeValue="N";
	$dtOpen->nodeValue="-";
	$usrOpen->nodeValue="-";
	$usr->nodeValue=$slogin_Username;
	$coment->nodeValue="-";
	$alt->nodeValue="-";
	$innerHtml->nodeValue="-";
	$link->nodeValue="-";
	$css->nodeValue=$_POST["css"];
	$dt->nodeValue=date("d-m-Y H:i:s");
	$xml->save("../".folderWork.$folderGroup."/objects.xml");
}
if ($_POST["typeSave"]=="escSaveBack"){
	$back=$xml->getElementsByTagName("obj")->item(0);
	$isOpen=$back->getElementsByTagName("isOpen")->item(0);
	$dtOpen=$back->getElementsByTagName("dtOpen")->item(0);
	$usrOpen=$back->getElementsByTagName("usrOpen")->item(0);
	$isOpen->nodeValue="N";
	$dtOpen->nodeValue="-";
	$usrOpen->nodeValue="-";
	$xml->save("../".folderWork.$folderGroup."/objects.xml");
}
if ($_POST["typeSave"]=="saveNewText"){
	$file = simplexml_load_file("../".folderWork.$folderGroup."/objects.xml");
	$num=$xml->getElementsByTagName("obj")->length;
	$maxId=0;
	for ($i=0; $i<$num; $i++) {	
		$back=$xml->getElementsByTagName("obj")->item($i);	
		$id=$back->getElementsByTagName("id")->item(0);
		if ((int)($id->nodeValue)>$maxId){$maxId=$id->nodeValue;}
	}
	$maxId++;
	$vDate=date("d-m-Y H:i:s");	
	$obj = $file->addChild("obj");
    $obj->addChild("id",$maxId);
    $obj->addChild("type","Text");
	$obj->addChild("isOpen","N");
	$obj->addChild("dtOpen","-");
	$obj->addChild("usrOpen","-");
	$obj->addChild("usr",$slogin_Username);
	$obj->addChild("coment","-");
	$obj->addChild("alt","-");
	$obj->addChild("innerHtml",$_POST["txt"]);
	$obj->addChild("link",$_POST["url"]);
	$obj->addChild("css",$_POST["css"]);
	$obj->addChild("dt",$vDate);
	$file->asXML("../".folderWork.$folderGroup."/objects.xml");
	echo json_encode(array("numero"=>$num,"id"=>$maxId, "vDate"=>$vDate));
}
if ($_POST["typeSave"]=="actNewText"){
	$nId=(string) $_POST["numId"];
	$nodes=$xml->getElementsByTagName("obj");
	for ($i = 1; $i < $nodes->length; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);
		$num=$back->getElementsByTagName("id")->item(0);
		$numId=(string)$num->nodeValue;
		if ($numId===$nId){
			$isOpen=$back->getElementsByTagName("isOpen")->item(0);
			$dtOpen=$back->getElementsByTagName("dtOpen")->item(0);
			$usrOpen=$back->getElementsByTagName("usrOpen")->item(0);
			$usr=$back->getElementsByTagName("usr")->item(0);
			$coment=$back->getElementsByTagName("coment")->item(0);
			$alt=$back->getElementsByTagName("alt")->item(0);
			$innerHtml=$back->getElementsByTagName("innerHtml")->item(0);
			$link=$back->getElementsByTagName("link")->item(0);
			$css=$back->getElementsByTagName("css")->item(0);
			$dt=$back->getElementsByTagName("dt")->item(0);
			$isOpen->nodeValue="N";
			$dtOpen->nodeValue="-";
			$usrOpen->nodeValue="-";
			$usr->nodeValue=$slogin_Username;
			$coment->nodeValue=$_POST["comen"];
			$alt->nodeValue=$_POST["alter"];
			$innerHtml->nodeValue=$_POST["txt"];
			$link->nodeValue=$_POST["url"];
			$css->nodeValue=$_POST["css"];
			$dt->nodeValue=date("d-m-Y H:i:s");
			$xml->save("../".folderWork.$folderGroup."/objects.xml");	
		}	
	}
}
if ($_POST["typeSave"]=="actNewEmbed"){
	$nId=(string) $_POST["numId"];
	$nodes=$xml->getElementsByTagName("obj");
	for ($i = 1; $i < $nodes->length; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);
		$num=$back->getElementsByTagName("id")->item(0);
		$numId=(string)$num->nodeValue;
		if ($numId===$nId){
			$isOpen=$back->getElementsByTagName("isOpen")->item(0);
			$dtOpen=$back->getElementsByTagName("dtOpen")->item(0);
			$usrOpen=$back->getElementsByTagName("usrOpen")->item(0);
			$usr=$back->getElementsByTagName("usr")->item(0);
			$coment=$back->getElementsByTagName("coment")->item(0);
			$alt=$back->getElementsByTagName("alt")->item(0);
			$innerHtml=$back->getElementsByTagName("innerHtml")->item(0);
			$link=$back->getElementsByTagName("link")->item(0);
			$css=$back->getElementsByTagName("css")->item(0);
			$dt=$back->getElementsByTagName("dt")->item(0);
			$isOpen->nodeValue="N";
			$dtOpen->nodeValue="-";
			$usrOpen->nodeValue="-";
			$usr->nodeValue=$slogin_Username;
			$css->nodeValue=$_POST["css"];
			$dt->nodeValue=date("d-m-Y H:i:s");
			$xml->save("../".folderWork.$folderGroup."/objects.xml");	
		}	
	}
}
if ($_POST["typeSave"]=="escActNewText"){
	$nId=intval($_POST["numId"]);
	$nodes=$xml->getElementsByTagName("obj");
	for ($i = 0; $i < $nodes->length; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);
		$num=$back->getElementsByTagName("id")->item(0);
		$numId=intval($num->nodeValue);
		if ($numId==$nId){
			$isOpen=$back->getElementsByTagName("isOpen")->item(0);
			$dtOpen=$back->getElementsByTagName("dtOpen")->item(0);
			$usrOpen=$back->getElementsByTagName("usrOpen")->item(0);
			$isOpen->nodeValue="N";
			$dtOpen->nodeValue="-";
			$usrOpen->nodeValue="-";
			$xml->save("../".folderWork.$folderGroup."/objects.xml");		
		}	
	}
}
if ($_POST["typeSave"]=="saveNewImage"){
	$file = simplexml_load_file("../".folderWork.$folderGroup."/objects.xml");
	$num=$xml->getElementsByTagName("obj")->length;
	$maxId=0;
	for ($i=0; $i<$num; $i++) {	
		$back=$xml->getElementsByTagName("obj")->item($i);	
		$id=$back->getElementsByTagName("id")->item(0);
		if ((int)($id->nodeValue)>$maxId){$maxId=$id->nodeValue;}
	}
	$maxId++;
	$vDate=date("d-m-Y H:i:s");	
	$obj = $file->addChild("obj");
    $obj->addChild("id",$maxId);
    $obj->addChild("type","Image");
	$obj->addChild("isOpen","N");
	$obj->addChild("dtOpen","-");
	$obj->addChild("usrOpen","-");
	$obj->addChild("usr",$slogin_Username);
	$obj->addChild("coment",$_POST["com"]);
	$obj->addChild("alt",$_POST["alt"]);
	$obj->addChild("innerHtml",$_POST["imgSRC"]);
	$obj->addChild("link",$_POST["url"]);
	$obj->addChild("css",$_POST["css"]);
	$obj->addChild("dt",$vDate);
	$file->asXML("../".folderWork.$folderGroup."/objects.xml");
	echo json_encode(array("numero"=>$num,"id"=>$maxId, "vDate"=>$vDate));
}
if ($_POST["typeSave"]=="saveNewMedia"){
	$file = simplexml_load_file("../".folderWork.$folderGroup."/objects.xml");
	$num=$xml->getElementsByTagName("obj")->length;
	$maxId=0;
	for ($i=0; $i<$num; $i++) {	
		$back=$xml->getElementsByTagName("obj")->item($i);	
		$id=$back->getElementsByTagName("id")->item(0);
		if ((int)($id->nodeValue)>$maxId){$maxId=$id->nodeValue;}
	}
	$maxId++;
	$vDate=date("d-m-Y H:i:s");	
	$obj = $file->addChild("obj");
    $obj->addChild("id",$maxId);
    $obj->addChild("type",$_POST["typeM"]);
	$obj->addChild("isOpen","N");
	$obj->addChild("dtOpen","-");
	$obj->addChild("usrOpen","-");
	$obj->addChild("usr",$slogin_Username);
	$obj->addChild("coment","-");
	$obj->addChild("alt","-");
	$obj->addChild("innerHtml",$_POST["iH"]);
	$obj->addChild("link","-");
	$obj->addChild("css",$_POST["css"]);
	$obj->addChild("dt",$vDate);
	$file->asXML("../".folderWork.$folderGroup."/objects.xml");
	echo json_encode(array("numero"=>$num,"id"=>$maxId, "vDate"=>$vDate));
}
if ($_POST["typeSave"]=="saveNewEmbed"){
	$file = simplexml_load_file("../".folderWork.$folderGroup."/objects.xml");
	$num=$xml->getElementsByTagName("obj")->length;
	$maxId=0;
	for ($i=0; $i<$num; $i++) {	
		$back=$xml->getElementsByTagName("obj")->item($i);	
		$id=$back->getElementsByTagName("id")->item(0);
		if ((int)($id->nodeValue)>$maxId){$maxId=$id->nodeValue;}
	}
	$maxId++;
	$codigo=str_replace('\"','||',$_POST["txt"]);
	$codigo=str_replace('<','*;',$codigo);
	$codigo=str_replace('>',';*',$codigo);
	$vDate=date("d-m-Y H:i:s");	
	$obj = $file->addChild("obj");
    $obj->addChild("id",$maxId);
    $obj->addChild("type","Embed");
	$obj->addChild("isOpen","N");
	$obj->addChild("dtOpen","-");
	$obj->addChild("usrOpen","-");
	$obj->addChild("usr",$slogin_Username);
	$obj->addChild("coment","-");
	$obj->addChild("alt","-");
	$obj->addChild("innerHtml",$codigo);
	$obj->addChild("link","-");
	$obj->addChild("css",$_POST["css"]);
	$obj->addChild("dt",$vDate);
	$file->asXML("../".folderWork.$folderGroup."/objects.xml");
	echo json_encode(array("numero"=>$num,"id"=>$maxId, "vDate"=>$vDate, "codigo"=>$_POST["txt"]));
}
if ($_POST["typeSave"]=="pngFile"){
	$file = simplexml_load_file("../".folderWork.$folderGroup."/objects.xml");
	$num=$xml->getElementsByTagName("obj")->length;
	$maxId=0;
	for ($i=0; $i<$num; $i++) {	
		$back=$xml->getElementsByTagName("obj")->item($i);	
		$id=$back->getElementsByTagName("id")->item(0);
		if ((int)($id->nodeValue)>$maxId){$maxId=$id->nodeValue;}
	}
	$maxId++;
	$vDate=date("d-m-Y H:i:s");	
	$obj = $file->addChild("obj");
    $obj->addChild("id",$maxId);
    $obj->addChild("type","Image");
	$obj->addChild("isOpen","N");
	$obj->addChild("dtOpen","-");
	$obj->addChild("usrOpen","-");
	$obj->addChild("usr",$slogin_Username);
	$obj->addChild("coment","-");
	$obj->addChild("alt","-");
	$obj->addChild("innerHtml",$_POST['imageData']);
	$obj->addChild("link","-");
	$obj->addChild("css",$_POST["css"]);
	$obj->addChild("dt",$vDate);
	$file->asXML("../".folderWork.$folderGroup."/objects.xml");
	echo json_encode(array("numero"=>$num,"id"=>$maxId, "vDate"=>$vDate));
}
?>

