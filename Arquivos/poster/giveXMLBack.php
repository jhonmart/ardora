$folderGroup=$_GET["folderG"];
$xml = new DOMDocument('1.0', 'utf-8');
$xml->formatOutput = true;
$xml->preserveWhiteSpace = false;
$xml->load("../".folderWork.$folderGroup."/objects.xml");
if ($_GET["typeObj"]=="isOpenBack"){
	$back=$xml->getElementsByTagName("obj")->item(0);
	$isOpen=$back->getElementsByTagName("isOpen")->item(0);
	$usrOpen=$back->getElementsByTagName("usrOpen")->item(0);
	$dtOpen=$back->getElementsByTagName("dtOpen")->item(0);
	echo json_encode(array("isOpen"=>$isOpen->nodeValue,"usrOpen"=>$usrOpen->nodeValue,"dtOpen"=>$dtOpen->nodeValue)); 
}
if ($_GET["typeObj"]=="isOpen"){
	$nId=(string) $_GET["numId"];	
	$file = simplexml_load_file("../".folderWork.$folderGroup."/objects.xml");
	$nodes = $file->xpath('//obj[id[.="'.$nId.'"]]');
	$isOpen = (string) $nodes[0]->isOpen;
	$usrOpen = (string) $nodes[0]->usrOpen;
	$dtOpen = (string) $nodes[0]->dtOpen;
	echo json_encode(array("isOpen"=>$isOpen,"usrOpen"=>$usrOpen,"dtOpen"=>$dtOpen)); 
}
if ($_GET["typeObj"]=="back"){ 
	$back=$xml->getElementsByTagName("obj")->item(0);
	$cssStyle=$back->getElementsByTagName("css")->item(0);
	echo json_encode(array("cssStyle"=>$cssStyle->nodeValue));
}
if ($_GET["typeObj"]=="list"){ 
	$listId="";
	$listType="";
	$listDt="";
	$num=$xml->getElementsByTagName("obj")->length;
	for ($i=0; $i<$num; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);	
		$id=$back->getElementsByTagName("id")->item(0);
		$type=$back->getElementsByTagName("type")->item(0);
		$dt=$back->getElementsByTagName("dt")->item(0);
		$listId=$listId.$id->nodeValue."|";
		$listType=$listType.$type->nodeValue."|";
		$listDt=$listDt.$dt->nodeValue."|";
	}
	echo json_encode(array("listId"=>$listId,"listType"=>$listType,"listDt"=>$listDt,"num"=>$num));
}
if ($_GET["typeObj"]=="objects"){ 
	$listId="";
	$listType="";
	$listAlt="";
	$listComent="";
	$listInnerHtml="";
	$listLink="";
	$listCss="";
	$num=$xml->getElementsByTagName("obj")->length;
	for ($i=1; $i<$num; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);	
		$id=$back->getElementsByTagName("id")->item(0);
		$type=$back->getElementsByTagName("type")->item(0);
		$alt=$back->getElementsByTagName("alt")->item(0);
		$coment=$back->getElementsByTagName("coment")->item(0);
		$innerHtml=$back->getElementsByTagName("innerHtml")->item(0);
		$link=$back->getElementsByTagName("link")->item(0);
		$cssStyle=$back->getElementsByTagName("css")->item(0);
		$listId=$listId.$id->nodeValue."~";
		$listType=$listType.$type->nodeValue."~";
		$listAlt=$listAlt.$alt->nodeValue."~";
		$listComent=$listComent.$coment->nodeValue."~";
		$listInnerHtml=$listInnerHtml.$innerHtml->nodeValue."~";
		$listLink=$listLink.$link->nodeValue."~";
		$listCss=$listCss.$cssStyle->nodeValue."~";	
	}
	echo json_encode(array("listId"=>$listId,"listType"=>$listType,"listAlt"=>$listAlt, "listComent"=>$listComent,"listInnerHtml"=>$listInnerHtml,"listLink"=>$listLink,"listCss"=>$listCss,"num"=>$num));
}
if ($_GET["typeObj"]=="getParaText"){
	$nId=intval($_GET["numId"]);
	$nodes=$xml->getElementsByTagName("obj");
	for ($i = 0; $i < $nodes->length; $i++) {
		$back=$xml->getElementsByTagName("obj")->item($i);
		$num=$back->getElementsByTagName("id")->item(0);
		$numId=intval($num->nodeValue);
		if ($numId==$nId){
			$innerHtml=$back->getElementsByTagName("innerHtml")->item(0);
			$link=$back->getElementsByTagName("link")->item(0);
			$css=$back->getElementsByTagName("css")->item(0);
			$alt=$back->getElementsByTagName("alt")->item(0);
			$coment=$back->getElementsByTagName("coment")->item(0);
			$innerHtml=$innerHtml->nodeValue;
			$link=$link->nodeValue;
			$css=$css->nodeValue;
			$alt=$alt->nodeValue;
			$coment=$coment->nodeValue;
		echo json_encode(array("IH"=>$innerHtml,"linkValue"=>$link,"cssStyle"=>$css,"altValue"=>$alt,"comentValue"=>$coment));
		}	
	}
}
?>


