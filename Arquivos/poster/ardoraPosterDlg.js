/*ardoraPosterDlg, created by ARDORA www.webardora.net

Copyright (C) <2011>  <Jos� Manuel Bouz�n M.>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
var isOpenTextTool=false;
var isSave=false;
$(function(){
		   
   var editPro=$("#editProperties").dialog({autoOpen: false,resizable:false,
//PROW
		 modal:true,buttons: {
//AC
         				archivo=document.eProperties.imaxenProper.value;
				extensiones_permitidas=new Array(".jpeg",".jpg",".gif",".png");
				extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
				permitida=false;

				if (archivo.length==0){permitida=true;}
				for (var i = 0; i < extensiones_permitidas.length; i++) {
					if (extensiones_permitidas[i] == extension) {
						permitida = true;break;
					}
				}
				if (!permitida){
document.getElementById("errorFileNAPro").innerHTML = '<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>"<strong>'+ extension+'</strong>" - File type not allowed</p>';
				  $("#errorFileNAPro").css("visibility","visible");
				  InitializeTimer();
				}
				else{
					$("#errorFileNAPro").css("visibility","hidden");						
					document.eProperties.target = "estadoUpLoadPro";
					document.eProperties.submit();
					var iFrame = document.getElementById("estadoUpLoadPro");
					var loading = document.getElementById("loadingPro");
					iFrame.style.display = "none";
					iFrame.contentDocument.getElementsByTagName("body")[0].innerHTML = "";
					loading.style.display = "block";
					$( this ).dialog( "close" );
				}		
			},
//CAN
					$( this ).dialog( "close" );
				}
		 	}
       });

       var backGroundDialog=$("#backGroundDialog").dialog({
         autoOpen: false,resizable:false,
//BGW
		 modal:true,
		 open: function(event, ui) {isSave=false},
		 close: function(event, ui) { 
		 	if (!isSave){
				$.post("saveXMLBack.php",{folderG:folderGroup,typeSave:"escSaveBack"},function(data){},"json");
			}
        },
		 buttons: {
//AC
                 	isSave=true;
					if ($("#haiImaxenBack").is(':checked')){
						if ($.trim($("#imaxenBack").val())!=""){ 
							archivo=document.entraFondo.imaxenBack.value;
							extensiones_permitidas=new Array(".jpeg",".jpg",".gif",".png");
							extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
							permitida=false;
							for (var i = 0; i < extensiones_permitidas.length; i++) {
								if (extensiones_permitidas[i] == extension) {
									permitida = true;break;
								}
							}
							if (!permitida){
//EFB								
				  $("#errorFileNABack").css("visibility","visible");
								InitializeTimer();
							}
							else{
								$("#errorFileNABack").css("visibility","hidden");						
								document.entraFondo.target = "estadoUpLoadBack";
								document.entraFondo.submit();
								var iFrame = document.getElementById("estadoUpLoadBack");
								var loading = document.getElementById("loadingBack");
								iFrame.style.display = "none";
								iFrame.contentDocument.getElementsByTagName("body")[0].innerHTML = "";
								loading.style.display = "block";
								newBackImage();
							}
						}
						else{ newBackOld();
						}
					}
					else{
						newBack();
					}
			},
//CAN
					$( this ).dialog( "close" );
				}
		 	}
       });
	   var borraDialog=$("#borrarDialog").dialog({
         autoOpen: false,resizable:false,
//BOW
		 modal:true,
		 buttons: {
//YES
				hideButtons();
				nId=parseInt($(selected).attr("id").substring(0,3),10);
				$.post("openEditXML.php",{folderG:folderGroup,typeObj:"deleteOBJ",numId:nId},function(data){},"json");
			 	selected.parentNode.removeChild(selected);	
				selected=null;
			 	$( this ).dialog( "close" );
			},
//NO
					$( this ).dialog( "close" );
				}
		 	}
       });
	   var editaDialog=$("#editarDialog").dialog({
         autoOpen: false,
//EDW
		 modal:true,resizable:false,
		 buttons: {
//GAR
			 if ($("#newItem").is(':checked')){
				document.editarTexto.texto.value=jQuery.trim(document.editarTexto.texto.value);
			 	newText(document.editarTexto.texto.value,true);
			 }
			 else{
				 selected.innerHTML=jQuery.trim(document.editarTexto.texto.value);
				 $(selected).css("font-family",$("#texto").css("font-family"));
				 $(selected).css("font-size",$("#texto").css("font-size"));
				 $(selected).css("font-weight",$("#texto").css("font-weight"));
				 $(selected).css("font-style",$("#texto").css("font-style"));
				 $(selected).css("color",$("#texto").css("color"));
				 var angulo=$("#xiro").val();
				 var cssObj = {
					"transform" : "rotate("+angulo+"deg)",
					"-ms-transform" : "rotate("+angulo+"deg)",
					"-webkit-transform" : "rotate("+angulo+"deg)",
					"-moz-transform" : "rotate("+angulo+"deg)",
					"-o-transform" : "rotate("+angulo+"deg)"
				};
				$(selected).css(cssObj);
				 $(selected).css("text-align",$("#texto").css("text-align"));
				 if ($("#sombraTexto").is(':checked')){
  			     	$(selected).css("text-shadow",$("#texto").css("text-shadow"));
				 }
				 else{
				 	$(selected).css("text-shadow","none");
				 }
				  if ($("#sombraBorde").is(':checked')){
						cssObj = {
							"-moz-box-shadow": "0 0 5px #888",
							"-webkit-box-shadow" : "0 0 5px#888",
							"box-shadow" : "0 0 5px #888"			
						}
				   }
				   else{
					cssObj = {
							  "-moz-box-shadow": "none",
							  "-webkit-box-shadow" : "none",
							  "box-shadow" : "none"
							}
				   }
				   $(selected).css(cssObj);
				 var opa=$("#transparencia").val();
				 var cssObj = {
					 "-ms-filter progid" : "DXImageTransform.Microsoft.Alpha(Opacity="+opa*100+")",
					 "filter" : "alpha(opacity="+opa*100+")",
					 "opacity" : opa
				 };
				 $(selected).css(cssObj);
				 $(selected).css("border-width",$("#borde").val()+"px");
				 if ($("#transparente").is(':checked')){
  			     	$(selected).css("background","transparent");
				 }
				 else{
				 	$(selected).css("background",$("#texto").css("background-color"));
				 }
				 if ($("#autoW").is(':checked')){
					 $(selected).css("width","auto");
					 $(selected).css("height","auto");
				   }
				   else{
					 $(selected).css("width",$("#textoAncho").val()+"px");
					 $(selected).css("height",$("#textoAlto").val()+"px");
				   }
				 $(selected).attr("title",$("#url").val());
			 }
			 $( this ).dialog( "close" );
			},
//CAN
					$( this ).dialog( "close" );
				}
		 	}
       });
	    var embedDialog=$("#embedNew").dialog({
         autoOpen: false,
//EMW
		 modal:true,
		 open: function(event, ui) { document.entraEmbed.codigoEmbed.value=""; },
		 buttons: {
//GAR
			 	document.entraEmbed.codigoEmbed.value=jQuery.trim(document.entraEmbed.codigoEmbed.value);
			 	newEmbed(document.entraEmbed.codigoEmbed.value);
			 $( this ).dialog( "close" );
			},
//CAN
					$( this ).dialog( "close" );
				}
		 	}
       });
	    var imageDialog=$("#imageNew").dialog({
         autoOpen: false,resizable:false,
//IMAW
		 modal:true,
		 open: function(event, ui) { document.entraImaxe.imaxen.value="";document.getElementById("estadoUpLoad").contentDocument.getElementsByTagName("body")[0].innerHTML = "";},
		 buttons: {
//GAR
				if ($("#newImg").is(':checked')){
					archivo=document.entraImaxe.imaxen.value;
					extensiones_permitidas=new Array(".jpeg",".jpg",".gif",".png");
					extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
					permitida=false;
					for (var i = 0; i < extensiones_permitidas.length; i++) {
						if (extensiones_permitidas[i] == extension) {
							permitida = true;break;
						}
					}
					if (!permitida){
//EFI
		  				$("#errorFileNAImg").css("visibility","visible");InitializeTimer();}
					else{
						$("#errorFileNAImg").css("visibility","hidden");
						document.entraImaxe.target = "estadoUpLoad";
						document.entraImaxe.submit();
						var iFrame = document.getElementById("estadoUpLoad");
						var loading = document.getElementById("loading");
						iFrame.style.display = "none";
						iFrame.contentDocument.getElementsByTagName("body")[0].innerHTML = "";
						loading.style.display = "block";
						newImage();
					}
				}
				else{
					var imaSel=selected.getElementsByTagName("img");
					$(imaSel).attr("longdesc",$("#comentaImaxen").val());
	 				$(imaSel).attr("alt",$("#altImaxen").val());
					var angulo=$("#xiroImg").val();
				 	var cssObj = {"transform" : "rotate("+angulo+"deg)","-ms-transform" : "rotate("+angulo+"deg)","-webkit-transform" : "rotate("+angulo+"deg)","-moz-transform" : "rotate("+angulo+"deg)","-o-transform" : "rotate("+angulo+"deg)"};
					$(selected).css("text-align",$("#fontAliImg").css("text-align"));
					$(selected).css(cssObj);
					var opa=$("#transparenciaImg").val();
  				    cssObj = {
						 "-ms-filter progid" : "DXImageTransform.Microsoft.Alpha(Opacity="+opa*100+")",
						 "filter" : "alpha(opacity="+opa*100+")",
						 "opacity" : opa
					};
					$(selected).css(cssObj);
					$(selected).css("border-width",$("#bordeImg").val()+"px");
					$(selected).css("color","#"+$("#corBordeImg").val());
					if ($("#transparenteImg").is(':checked')){$(selected).css("background","transparent");}
					else{$(selected).css("background","#"+$("#corFondoImg").val());}
					if ($("#sombraBordeImg").is(':checked')){cssObj = {"-moz-box-shadow": "0 0 5px #888","-webkit-box-shadow" : "0 0 5px#888","box-shadow" : "0 0 5px #888"}
					}else{cssObj = {"-moz-box-shadow": "none","-webkit-box-shadow" : "none","box-shadow" : "none"}}
				    $(selected).css(cssObj);$(selected).attr("title",$("#urlImaxen").val());$( this ).dialog( "close" );
				}},
//CAN
					$( this ).dialog( "close" );
				}
		 	}
       });
	   
	   var mediaDialog=$("#mediaNew").dialog({
         autoOpen: false,resizable:false,
//MEDIAW
		 modal:true,
		 open: function(event, ui) { 
		 	document.entraMedia.media.value="";
			document.getElementById("estadoUpLoadMedia").contentDocument.getElementsByTagName("body")[0].innerHTML = "";
		 },
		 buttons: {
//GAR
				archivo=document.entraMedia.media.value;
				
				if ($("#isAudio").is(':checked')){extensiones_permitidas=new Array(".mp3",".oga",".ogg");}
				else{extensiones_permitidas=new Array(".mp4",".ogv",".ogg", ".flv");}
				extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
				permitida=false;
				for (var i = 0; i < extensiones_permitidas.length; i++) {
					if (extensiones_permitidas[i] == extension) {
						permitida = true;break;
					}
				}
				if (!permitida){
//EF
		  $("#errorFileNA").css("visibility","visible");
						InitializeTimer();
				}
				else{
						$("#errorFileNA").css("visibility","hidden");
						document.entraMedia.target = "estadoUpLoadMedia";
						document.entraMedia.submit();
						var iFrame = document.getElementById("estadoUpLoadMedia");
						var loading = document.getElementById("loadingMedia");
						iFrame.style.display = "none";
						iFrame.contentDocument.getElementsByTagName("body")[0].innerHTML = "";
						loading.style.display = "block";
						newMedia();
				}
			},
//CAN
					$( this ).dialog( "close" );
				}
		 	}
       });
	   $( "#openTextTool" ).button({
            icons: {primary: "ui-icon-arrowthickstop-1-e"},
            text: true
        }).click(function(event){
			if (isOpenTextTool){
				$( "#openTextTool" ).button("option", {
			  icons: { primary: "ui-icon-arrowthickstop-1-e" }
				});
				$( "#areaTextTool" ).animate({
					width: "0px",
					height: "0px"
				  }, 700, function() {
					$("#opaMais").css({"visibility" : "hidden"});
					$("#opaMenos").css({"visibility" : "hidden"});
					$("#Agrande").css({"visibility" : "hidden"});
					$("#Apeque").css({"visibility" : "hidden"});
					$("#bold").css({"visibility" : "hidden"});
					$("#italic").css({"visibility" : "hidden"});
					$("#fontColor").css({"visibility" : "hidden"});
					$("#border0").css({"visibility" : "hidden"});
					$("#border1").css({"visibility" : "hidden"});
					$("#backColor").css({"visibility" : "hidden"});
					$("#backTrans").css({"visibility" : "hidden"});
					$("#text_l").css({"visibility" : "hidden"});
					$("#text_r").css({"visibility" : "hidden"});
					$("#text_c").css({"visibility" : "hidden"});
					$("#text_j").css({"visibility" : "hidden"});
				  })
				isOpenTextTool=false;
			}
			else{
				$( "#openTextTool" ).button("option", {
			  icons: { primary: "ui-icon-arrowthickstop-1-w" }
				});
				$( "#areaTextTool" ).animate({
					width: "159px",
					height: "73px"
				  }, 700, function() {
      				  $("#opaMais").css({"visibility" : "visible"});
					  $("#opaMenos").css({"visibility" : "visible"});
					  $("#Agrande").css({"visibility" : "visible"});
					  $("#Apeque").css({"visibility" : "visible"});
					  $("#bold").css({"visibility" : "visible"});
					  $("#italic").css({"visibility" : "visible"});
					  $("#fontColor").css({"visibility" : "visible"});
					  $("#border0").css({"visibility" : "visible"});
					  $("#border1").css({"visibility" : "visible"});
					  $("#backColor").css({"visibility" : "visible"});
					  $("#backTrans").css({"visibility" : "visible"});
					  $("#text_l").css({"visibility" : "visible"});
					  $("#text_r").css({"visibility" : "visible"});
					  $("#text_c").css({"visibility" : "visible"});
					  $("#text_j").css({"visibility" : "visible"});
					// Animation complete.
				  })
				isOpenTextTool=true;	
			}
        });
});