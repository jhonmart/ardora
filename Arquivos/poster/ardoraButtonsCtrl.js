/*ardoraButtonsCtrl, created by ARDORA www.webardora.net

Copyright (C) <2011>  <Jos� Manuel Bouz�n M.>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* ctrl BUTTONS ===================== */
$(function(){$(".fg-button:not(.ui-state-disabled)").hover(function(){ $(this).addClass("ui-state-hover");}, function(){ $(this).removeClass("ui-state-hover");})
.mousedown(function(){
   $(this).parents(".fg-buttonset-single:first").find(".fg-button.ui-state-active").removeClass("ui-state-active");
   if( $(this).is(".ui-state-active.fg-button-toggleable, .fg-buttonset-multi .ui-state-active") ){ $(this).removeClass("ui-state-active"); }
   else { $(this).addClass("ui-state-active"); }})
.mouseup(function(){
   if(! $(this).is(".fg-button-toggleable, .fg-buttonset-single .fg-button,  .fg-buttonset-multi .fg-button") ){
   $(this).removeClass("ui-state-active");}});
$(".ui-button:not(.ui-state-disabled)")
.hover(
function(){ $(this).addClass("ui-state-hover");},
function(){ $(this).removeClass("ui-state-hover"); }
)
.mousedown(function(){
   $(this).parents(".ui-buttonset-single:first").find(".ui-button.ui-state-active").removeClass("ui-state-active");
   if( $(this).is(".ui-state-active.fg-button-toggleable, .ui-buttonset-multi .ui-state-active") ){ $(this).removeClass("ui-state-active"); }
   else { $(this).addClass("ui-state-active"); }})
.mouseup(function(){
   if(! $(this).is(".ui-button-toggleable, .ui-buttonset-single .ui-button,  .ui-buttonset-multi .ui-button") ){
   $(this).removeClass("ui-state-active");}});
});
/* end BUTTONS ===================== */