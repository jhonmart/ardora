/*Creado con Ardora - www.webardora.net';
  bajo licencia Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
  para otros usos contacte con el autor  
*/
father= new Array(); var isFull=false; 
var seleJob="N"; var isRefresh=true; var pathFolder="";
a_dtObjects= new Array(); n_dtObjects= new Array();aId= new Array(); n_Id= new Array();
$(document).ready( function(){
//DR						
  	var erroOF=$("#errorOpenFolder").dialog({
         autoOpen: true,width: 400,height: "auto",modal:true,
		 buttons: {
//AC
		 location.href="index.php";$( this ).dialog( "close" );}}});	
  $("#bFullW").click(function (){if (isFull) {isFull=false;fullScreenCancel();}else{isFull=true;fullScreen()}});  
  $("#bNewText").click( function(){ openJob(seleJob);});
  $("#bShowSC").click( function(){hideSC();}); 
  $("#bMoveSC_R").click( function(){scrollMove("R");});
  $("#bMoveSC_L").click( function(){ scrollMove("L");});
  $("#scMenu a").each(function (index) {if ($(this).attr("id").substring(0,5)=="openJ"){ $(this).click( function(){seleJob=$(this).attr("id").substring(5);jobShow($(this).attr("id").substring(5));});if (seleJob=="N"){seleJob=$(this).attr("id").substring(5);jobShow($(this).attr("id").substring(5));};}});
  $("#bCloseButtonTopBarShow").click( function(){$( "#botBarShow" ).animate({height: "0px"}, 500, function() {})});
  $("#botBarShow").mouseenter(function(e){$( "#botBarShow" ).animate({height: "25px"}, 500, function() {})});
});

$(function(){
	var div = $('div.sc_menu'),
		ul = $('ul.sc_menu'),
		ulPadding = 15;
	var divWidth = div.width();
	div.css({overflow: 'hidden'});
	var lastLi = ul.find('li:last-child');
	div.mousemove(function(e){
		var ulWidth = lastLi[0].offsetLeft + lastLi.outerWidth() + ulPadding;	
		var left = (e.pageX - div.offset().left) * (ulWidth-divWidth) / divWidth;
		div.scrollLeft(left);
	});
});
function scrollMove(val){
	var posScroll=$('div.sc_menu').scrollLeft()
	if (val=="R"){
		posScroll=posScroll+100;
	}
	else{
		posScroll=posScroll-100;
		if (posScroll<0){posScroll=0;}
	}
	$('div.sc_menu').animate({scrollLeft:posScroll}, "fast");
}
function hideSC(){
	if ($("div.sc_menu").css("visibility")== "hidden"){
		$( "#bShowSC span" ).removeClass("ui-icon-arrowstop-1-s");
		$( "#bShowSC span" ).addClass("ui-icon-arrowstop-1-n");
		$( "#scScroll" ).animate({height: "115px"}, 500, function() {$("div.sc_menu").css({"visibility" : "visible"});$("#bMoveSC_R").css({"visibility" : "visible"});$("#bMoveSC_L").css({"visibility" : "visible"});})
	}
	else{
		$( "#bShowSC span" ).removeClass("ui-icon-arrowstop-1-n");
		$( "#bShowSC span" ).addClass("ui-icon-arrowstop-1-s");
		$( "#scScroll" ).animate({
					height: "0px"
				  }, 500, function() {
						$("div.sc_menu").css({"visibility" : "hidden"});
						$("#bMoveSC_R").css({"visibility" : "hidden"});
						$("#bMoveSC_L").css({"visibility" : "hidden"});
		})
		
	}
}
function openJob(num){
	window.location.href = "php/openPoster.php?num="+num;
}
function jobShow(num){
	document.oncontextmenu = function() {return false;};
	var a=document.getElementById("pizarraShow");
 	while(a.hasChildNodes()) a.removeChild(a.firstChild);
	a.innerHTML='<div id="addTextNew"  title="http://"></div>'+ 
	'<div id="addEmbedNew" title="http://"></div>'+
	'<div id="addImageNew" title="http://">'+
	'<span style=" display: inline-block; margin-right: -1px; vertical-align: middle; height:0px; width: 1px; visibility:hidden;"></span></div>'+
	'<div id="addMediaNew" title=""></div>'+
	'<div id="addImgShow" title=""></div>'
	$.ajaxSetup({async:false});
	$.get("php/giveXMLShow.php",{typeObj:"getPathShow", num:num},function(data){
			document.getElementById("jobTitle").innerHTML=$.trim(data.tit);
			pathFolder=$.trim(data.fN);
	}, "json");
	$.ajaxSetup({async:true});
	$.get("php/giveXMLShow.php",{typeObj:"back", path:pathFolder},function(data){
			data.cssStyle=data.cssStyle.replace("../ardoraWorkFiles","ardoraWorkFiles"); doStyle("pizarraShow",data.cssStyle);								 
   	}, "json");
	$.get("php/giveXMLShow.php",{typeObj:"objects", path:pathFolder},function(data){
			var vecId=data.listId.split("~");
			aId=data.listId.split("~");
			a_dtObjects=data.listDt.split("~");
			var vecType=data.listType.split("~");
			var vecAlt=data.listAlt.split("~");
			var vecCom=data.listComent.split("~");
			var vecIH=data.listInnerHtml.split("~");
			var vecLink=data.listLink.split("~");
			var vecCss=data.listCss.split("~");
			for (var i = 0; i < data.num; i++) {
				if (vecType[i]=="Text"){
					var pre=String(vecId[i]);
					while (pre.length<3){pre="0"+pre;}
					nT=newShowText(vecIH[i],pre);
					$(nT).attr("id",pre+"_Text");
					$(nT).attr("title",vecLink[i]);
					doStyle(pre+"_Text",vecCss[i],vecType[i]);
				}
				if (vecType[i]=="Image"){
					if (vecIH[i].substring(0,2)==".."){newImgBack=document.createElement("img");$(newImgBack).addClass("hiddenpic"); $(newImgBack).attr("src",vecIH[i].replace("thumbs","original").replace("../ardoraWorkFiles","ardoraWorkFiles"));}
					nT=newShowImage(vecId[i],vecAlt[i],vecCom[i],vecIH[i],vecLink[i],vecCss[i]);
				}
				if (vecType[i]=="Audio"){nT=newShowMedia(vecId[i],vecIH[i].replace("../ardoraWorkFiles","ardoraWorkFiles"),vecCss[i],"Audio");}
				if (vecType[i]=="Video"){nT=newShowMedia(vecId[i],vecIH[i].replace("../ardoraWorkFiles","ardoraWorkFiles"),vecCss[i],"Video");}
				if (vecType[i]=="Embed"){nT=newShowEmbed(vecId[i],recodificaIH(vecIH[i]),vecCss[i]);}
			}
   		}, "json");
		
}
function newShowText(textAdd,num){
   father[num]="addTextNew";
   var element=$("#addTextNew").clone();
   element.addClass("tempclass");
   $("#pizarraShow").append(element);
   $(".tempclass").attr("id","cloneNewText"+num);
   $("#cloneNewText"+num).removeClass("tempclass");
   fatherDIV=document.getElementById(father[num]);
   newDiv=document.getElementById("cloneNewText"+num);
   document.getElementById("cloneNewText"+num).innerHTML=textAdd;
   return $(newDiv); 
}
function newShowImage(num,vecAlt,vecCom,vecIH,vecLink,vecCss){
	father[num]="addImageNew";
   	var element=$("#addImageNew").clone(); 
    element.addClass("tempclass");
   	$("#pizarraShow").append(element);
   	$(".tempclass").attr("id","cloneNewImage"+num);
   	$("#cloneNewImage"+num).removeClass("tempclass");
   	fatherDIV=document.getElementById(father[num]);
   	newDiv=document.getElementById("cloneNewImage"+num);
	$(newDiv).attr("title",vecCom);
	if ((vecLink!="http://") && (vecLink!="-")) {
			$(newDiv).css('cursor','pointer');
			$(newDiv).bind("click", function() {		  
   				window.open(vecLink,"ww","width=1000,height=700,scrollbars=YES")
			}); 
	}
	else{$(newDiv).bind("click", function() {openImg(vecIH.replace("thumbs","original").replace("../ardoraWorkFiles","ardoraWorkFiles"));});}
var path=document.URL;path=path.substring(0,path.lastIndexOf("/"));path=path.substring(0,path.lastIndexOf("/"));path=path+vecIH.substring(2,vecIH.length);path=path.replace("thumbs","original");
	newImg=document.createElement("img");
    newImg.setAttribute("id", "IMG_drag"+num);
	newImg.setAttribute("alt",vecAlt);
	newImg.setAttribute("longdesc",vecCom);
	var cssObj1= {"display":"table-cell","vertical-align":"middle"};
	$(newImg).css(cssObj1);
	newImg.src=vecIH.replace("../ardoraWorkFiles","ardoraWorkFiles");
    newDiv.appendChild(newImg);var pre=String(num);while (pre.length<3){pre="0"+pre;}$(newDiv).attr("id",pre+"_Image");doStyle(pre+"_Image",vecCss);return $(newDiv);
}
function newShowEmbed(num, codigo, vecCss){
   father[num]="addEmbedNew";
   var element=$("#addEmbedNew").clone();
   element.addClass("tempclass");
   $("#pizarraShow").append(element);
   $(".tempclass").attr("id","cloneNewEmbed"+num);
   $("#cloneNewEmbed"+num).removeClass("tempclass");
   fatherDIV=document.getElementById(father[num]);
   newDiv=document.getElementById("cloneNewEmbed"+num);
   document.getElementById("cloneNewEmbed"+num).innerHTML=codigo;
   var pre=String(num);
   while (pre.length<3){pre="0"+pre;}
   $(newDiv).attr("id",pre+"_Embed");
   doStyle(pre+"_Embed",vecCss);
	return $(newDiv);
}

function newShowMedia(num,vecIH,vecCss,typeMedia){
 	father[num]="addMediaNew";
   	var element=$("#addMediaNew").clone(); 
    element.addClass("tempclass");
   	$("#pizarraShow").append(element);
   	$(".tempclass").attr("id","cloneNewMedia"+num);
   	$("#cloneNewMedia"+num).removeClass("tempclass");
   	fatherDIV=document.getElementById(father[num]);
   	newDiv=document.getElementById("cloneNewMedia"+num);
	newB=document.createElement("div");
	newB.setAttribute("id", "aButton_"+num);
	newB.setAttribute("class", "buttonTop fg-button ui-state-default fg-button-icon-solo ui-corner-all");
	newDiv.appendChild(newB);
	newSPAN=document.createElement("span");
	newB.appendChild(newSPAN);
	var pre=String(num);
	while (pre.length<3){pre="0"+pre;}
	$(newDiv).attr("id",pre+"_"+typeMedia);
	doStyle(pre+"_"+typeMedia,vecCss);
	$(newDiv).css("border","none");
    if (typeMedia=="Audio"){
		newSPAN.setAttribute("class","ui-icon ui-icon-volume-on");
		$(newDiv).bind("click", function() {
				bm=document.getElementById(pre+"_"+typeMedia);						 
				openAudioPlayer(vecIH,$(bm).css("top"),$(bm).css("left"));
		});
	}
	else{
		newSPAN.setAttribute("class","ui-icon ui-icon-video");
		$(newDiv).bind("click", function() {
				bm=document.getElementById(pre+"_"+typeMedia);						 
				openVideoPlayer(vecIH,$(bm).css("top"),$(bm).css("left"));
   				
		});
	}
	return $(newDiv); 
}
function openAudioPlayer(file, topCss, leftCss){
	pizarra = document.getElementById("pizarraShow");
	if( document.getElementById("player")){pizarra.removeChild(document.getElementById("player"));}
	else{
		contenedor = document.createElement('div');
		contenedor.id ="player";
		pizarra.appendChild(contenedor);  
        ext = (file.substring(file.lastIndexOf("."))).toLowerCase(); 	
        htmlPlayer='<audio controls>';
        if (ext==".mp3"){
            htmlPlayer=htmlPlayer+'<source src="'+file+'" type="audio/mpeg">';
        }
        if ((ext==".oga") || (ext==".ogg")) {
            htmlPlayer=htmlPlayer+'<source src="'+file+'" type="audio/ogg">';
		}    
        htmlPlayer=htmlPlayer+'</audio>'; 
		document.getElementById("player").innerHTML=htmlPlayer;
		leftValue=parseInt(leftCss.replace("px",""));
		topValue=parseInt(topCss.replace("px",""));
		pizarraWidth=parseInt($("#pizarraShow").css("width").replace("px",""));
		pizarraHeight=parseInt($("#pizarraShow").css("height").replace("px",""));
		leftValue=leftValue+25;
		if (leftValue+350>pizarraWidth){
			leftValue=pizarraWidth-330;
		}
		topValue=topValue+20;
		if (topValue+48>pizarraHeight){
			topValue=pizarraHeight-48;
		}
		$("#player").css("width","300px");
		$("#player").css("top",topValue+"px");
		$("#player").css("left",leftValue+"px");
		$("#player").css("z-index",giveZindex());
	}
}
function openVideoPlayer(file, topCss, leftCss){
	pizarra = document.getElementById("pizarraShow");
	if( document.getElementById("player")){pizarra.removeChild(document.getElementById("player"));}
	else{
		ext = (file.substring(file.lastIndexOf("."))).toLowerCase();
	  if ((ext==".ogv") || (ext==".mp4") || (ext==".m4v")){
		contenedor = document.createElement('div');
		contenedor.id ="player";
		pizarra.appendChild(contenedor);	
		htmlPlayer='<video width="290" height="215" controls>';
        if (ext==".ogv"){htmlPlayer=htmlPlayer+'<source src="'+file+'" type="video/ogg">';}
		if ((ext==".m4v") || (ext==".mp4")){htmlPlayer=htmlPlayer+'<source src="'+file+'" type="video/mp4">';}
        htmlPlayer=htmlPlayer+'</video>';
		document.getElementById("player").innerHTML=htmlPlayer;
 	    leftValue=parseInt(leftCss.replace("px",""));
		topValue=parseInt(topCss.replace("px",""));
		pizarraWidth=parseInt($("#pizarraShow").css("width").replace("px",""));
		pizarraHeight=parseInt($("#pizarraShow").css("height").replace("px",""));
		leftValue=leftValue+25;
		if (leftValue+350>pizarraWidth){leftValue=pizarraWidth-330;}
		topValue=topValue+20;
		if (topValue+48>pizarraHeight){topValue=pizarraHeight-48;}
			$("#player").css("width","300px");
			$("#player").css("top",topValue+"px");
			$("#player").css("left",leftValue+"px");
			$("#player").css("z-index",giveZindex());
		}
	}
}
function doStyle(object,cssStyle,typeOBJ){
	var element=document.getElementById(object);
	var type=0;
	var vec=cssStyle.split("|");
	for(i=0;i<(vec.length-1);i++){
		var eleStyle=vec[i].split(":");
		if (eleStyle.length==2){
			$(element).css(eleStyle[0],eleStyle[1]);
		}
		else{
			$(element).css(eleStyle[0],eleStyle[1]+":"+eleStyle[2]);
		}
	}
	if (typeOBJ=="Text"){
		if ($(element).attr("title")!="http://"){
			var destino=$(element).attr("title");
			$(element).css('cursor','pointer');
			$(element).bind("click", function() {		  
   				window.open(destino,"ww","width=1000,height=700,scrollbars=YES")
			}); 
		}
		else{
			$(element).attr("title","");
		}
	}
	if(element.id!="pizarraShow"){
		$(element).css("position", "absolute");
	}
}
function recodificaIH(codigo){
	do {
    	codigo = codigo.replace("*;","<");
	} while(codigo.indexOf("*;") >= 0);
	do {
    	codigo = codigo.replace(";*",">");
	} while(codigo.indexOf(";*") >= 0);
	
	do {
    	codigo = codigo.replace("||",'"');
	} while(codigo.indexOf("||") >= 0);
	return codigo;
}
function giveZindex(){
	var valueZindex=0;
	capas=document.getElementsByTagName('div');
	for (i=0;i<capas.length;i++){
		if (parseInt($(capas[i]).css("z-index"),10)>valueZindex){
				if ($(capas[i]).attr("id").substring(3,4)=="_"){ 
					valueZindex=parseInt($(capas[i]).css("z-index"),10);
				}
		}
	}
	return valueZindex;
}
function openImg(vecIH){
   	var element=$("#addImgShow").clone(); 
    element.addClass("tempclass");
   	$("#pizarraShow").append(element);
   	$(".tempclass").attr("id","cloneNewImgShow");
   	$("#cloneNewImgShow").css("z-index",giveZindex()+1);
	$("#cloneNewImgShow").removeClass("tempclass");
   	fatherDIV=document.getElementById("addImgShow");
   	newDiv=document.getElementById("cloneNewImgShow");
	newImg=document.createElement("img");
    $(newImg).attr("src",vecIH);
	newImg.setAttribute("id", "IMG_show");
	var cssObj1= {"display":"table-cell",
	"vertical-align":"middle",
	"background-color":"#000000",
	"position":"absolute",
	"top":"0px",
	"left":"0px"};
	$(newImg).css(cssObj1);
	newDiv.appendChild(newImg);
	var posX=Math.floor(parseInt($(newDiv).css("width")) / 2)-Math.floor(parseInt($(newImg).css("width")) / 2);
	$(newImg).css("left",posX);
	var posY=Math.floor(parseInt($(newDiv).css("height")) / 2)-Math.floor(parseInt($(newImg).css("height")) / 2);
    $(newImg).css("top",posY);
	$(newDiv).bind("click", function() {
				newDiv.parentNode.removeChild(newDiv);
	});	
}
function doRefresh(){
	if (isRefresh){
//RECH1		
		$("#bRefresh").removeClass("ui-priority-secondary");
		$("#bRefresh").addClass("ui-priority-primary");
		isRefresh=false;
	}
	else{
//RECH2	
		$("#bRefresh").removeClass("ui-priority-primary");
		$("#bRefresh").addClass("ui-priority-secondary");
		isRefresh=true;
	}
}
function reFresh(){
	$("#bRefresh").addClass("ui-state-hover");
	checkObjects();
	$("#bRefresh").removeClass("ui-state-hover");
}
function checkObjects(){
	$.get("php/giveXMLShow.php",{typeObj:"objects", path:pathFolder},function(data){
			var vecId=data.listId.split("~");
			n_Id=data.listId.split("~");
			n_dtObjects=data.listDt.split("~");
			var vecType=data.listType.split("~");
			var vecAlt=data.listAlt.split("~");
			var vecCom=data.listComent.split("~");
			var vecIH=data.listInnerHtml.split("~");
			var vecLink=data.listLink.split("~");
			var vecCss=data.listCss.split("~");
			if (a_dtObjects[0]!=n_dtObjects[0]){
				$.get("php/giveXMLShow.php",{typeObj:"back", path:pathFolder},function(data){
						doStyle("pizarraShow",data.cssStyle);
   				}, "json");
			}
			for (var i = 1; i < a_dtObjects.length; i++) {
				var exists=false;
				for (var z = 1; z < n_dtObjects.length; z++) {
					if (n_Id[z]==aId[i]){
						exists=true;
						if (n_dtObjects[z]!=a_dtObjects[i]){							
							if (vecType[z]=="Text"){
								 nId=parseInt(n_Id[z],10);
								 var nameObject=String(n_Id[z]);
								 while (nameObject.length<3){nameObject="0"+nameObject;}
								 nameObject=nameObject+"_Text";
								 document.getElementById(nameObject).innerHTML=vecIH[z];
								 $("#"+nameObject).attr("title",vecLink[z]);
								 doStyle(nameObject,vecCss[z]);
							}
							if (vecType[z]=="Image"){
								 nId=parseInt(n_Id[z],10);
								 var nameObject=String(n_Id[z]);
								 while (nameObject.length<3){nameObject="0"+nameObject;}
								 nameObject=nameObject+"_Image";
								 var ele= document.getElementById(nameObject)
								 var imaSel=ele.getElementsByTagName("img");
								 $(imaSel).attr("src",vecIH[z]);
								 $(imaSel).attr("alt",vecAlt[z]);
								 $(imaSel).attr("longdesc",vecCom[z]);
								 $("#"+nameObject).attr("title",vecLink[z]);
								 doStyle(nameObject,vecCss[z]);
							}
							if (vecType[z]=="Audio"){
								 nId=parseInt(n_Id[z],10);
								 var nameObject=String(n_Id[z]);
								 while (nameObject.length<3){nameObject="0"+nameObject;}
								 nameObject=nameObject+"_Audio";
								 doStyle(nameObject,vecCss[z]);
							}
							if (vecType[z]=="Video"){
								 nId=parseInt(n_Id[z],10);
								 var nameObject=String(n_Id[z]);
								 while (nameObject.length<3){nameObject="0"+nameObject;}
								 nameObject=nameObject+"_Video";
								 doStyle(nameObject,vecCss[z]);
							}
							if (vecType[z]=="Embed"){
								 nId=parseInt(n_Id[z],10);
								 var nameObject=String(n_Id[z]);
								 while (nameObject.length<3){nameObject="0"+nameObject;}
								 nameObject=nameObject+"_Embed";
								 doStyle(nameObject,vecCss[z]);
							}
						}
					}
				}
				if (!exists){
					nId=parseInt(aId[i],10);
					var nameObject=String(aId[i]);
					while (nameObject.length<3){nameObject="0"+nameObject;}
					var resultado=document.getElementById(nameObject+"_Text");
					if (resultado!=null){resultado.parentNode.removeChild(resultado);}
					resultado=document.getElementById(nameObject+"_Image");
					if (resultado!=null){resultado.parentNode.removeChild(resultado);}
					resultado=document.getElementById(nameObject+"_Audio");
					if (resultado!=null){resultado.parentNode.removeChild(resultado);}
					resultado=document.getElementById(nameObject+"_Video");
					if (resultado!=null){resultado.parentNode.removeChild(resultado);}
					resultado=document.getElementById(nameObject+"_Embed");
					if (resultado!=null){resultado.parentNode.removeChild(resultado);}
				}
			}
			if (n_dtObjects.length>1){
			for (var z = 1; z < n_dtObjects.length; z++) {
				var exists=false;
				for (var i = 1; i < a_dtObjects.length; i++) {if (n_Id[z]==aId[i]){exists=true;}}
				if (!exists){
					$.get("php/giveXMLShow.php",{typeObj:"giveMeObject", path:pathFolder, numId:n_Id[z]},function(data){
						var vecType=data.listType;
						var vecAlt=data.listAlt;
						var vecCom=data.listComent;
						var vecIH=data.listInnerHtml;
						var vecLink=data.listLink;
						var vecCss=data.listCss;
						var Idi=data.numId;
						if (vecType=="Text"){
							var pre=String(Idi);
							while (pre.length<3){pre="0"+pre;}
							nT=newShowText(vecIH,pre);
							$(nT).attr("id",pre+"_Text");
							$(nT).attr("title",vecLink);
							doStyle(pre+"_Text",vecCss,vecType);
						}
						if (vecType=="Image"){
							if (vecIH.substring(0,2)==".."){
								newImgBack=document.createElement("img");
								$(newImgBack).addClass("hiddenpic");
								$(newImgBack).attr("src",vecIH.replace("thumbs","original"));
							}
						nT=newShowImage(Idi,vecAlt,vecCom,vecIH,vecLink,vecCss);
						}
						if (vecType=="Audio"){nT=newShowMedia(Idi,vecIH,vecCss,"Audio");}
						if (vecType=="Video"){nT=newShowMedia(Idi,vecIH,vecCss,"Video");}
						if (vecType=="Embed"){nT=newShowEmbed(Idi,recodificaIH(vecIH),vecCss);}
					}, "json");
				}
			  }
			}
		a_dtObjects=n_dtObjects.slice(0);
		aId=n_Id.slice(0);
	 }, "json");
	
}

function fullScreen() {
var docElement, request;docElement = document.getElementById("posterShow");
request = docElement.requestFullScreen || docElement.webkitRequestFullScreen || docElement.mozRequestFullScreen || docElement.msRequestFullScreen;
if (typeof request != "undefined" && request) {request.call(docElement);}}

function fullScreenCancel() {
var docElement, request;docElement = document;
request = docElement.cancelFullScreen || docElement.webkitCancelFullScreen || docElement.mozCancelFullScreen || docElement.msCancelFullScreen || docElement.exitFullscreen;
if (typeof request != "undefined" && request) {request.call(docElement);}} 