-----BEGIN PGP SIGNED MESSAGE-----


Simple Text-File Login (SiTeFiLo).
Copyright �2004,2005,2006 by Mario A. Valdez-Ramirez
http://www.mariovaldez.net/

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

You can contact Mario A. Valdez-Ramirez by email
at mario@mariovaldez.org or paper mail at
Olmos 809, San Nicolas, NL. 66495, Mexico.

=========================================
2006/01/04.


=========================================
* About the Simple Text-File Login script v1.0.6.

This is a simple authentication script written in PHP to be used in
small websites without SQL database, using a simple text file to
store the users and passwords.


The main page of the SiTeFiLo script is:
http://www.mariovaldez.net/software/sitefilo/


=========================================
* Features of the Simple Text-File Login script.

* Users/passwords are stored in simple text file (no SQL database
needed).
* Easy to integrate with the look and feel of your website.
* Optional code for common header and footer.
* Optional logging of login attempts.
* Optional MD5 hashed (encrypted) passwords.
* Perfect for few users (<100 users).
* Tested with IIS (Windows) and Apache (Windows and Linux).
* Simple to install.
* Open source. Released under the GPL license.

The SiTeFiLo script is perfect solution for small sites without SQL
server which require user authentication and login logs for less than
100 users.


=========================================
* Download the Simple Text-File Login script.

The SiTeFiLo script is available as a tar.gz package or as a zip file
at
http://www.mariovaldez.net/software/sitefilo/files/

Also you can find ther the Readme (readme.txt), Changelog
(version.txt), checksums (checksums.txt) and license (license.txt)
files.
 

=========================================
* Web forums about the Simple Text-File Login script.

There is a web forum for general discussion. If you have a question,
answer, suggestion or bug report post a comment. This is an open
forum, you are not required to register to read or post.
http://www.mariovaldez.net/webapps/forums/viewforum.php?f=11
 

=========================================
* Questions, comments, suggestions.

Don't hesitate to contact me by email (mario@mariovaldez.org).






-----BEGIN PGP SIGNATURE-----
Version: PGP 8.0

iQCVAwUBQ7uJ6zpHuYe8JiHtAQENCwQA1oO1Msa8dwBIRk1/UfTw6kTV/xLcAPQW
EueYy8rWw0IuQ85m/l/R69YF9swq42ytBd4kynaz5XBMoivjEdZ7JiOGGKLRlAGs
jwvtR83jdii3bLnDRmPTYAZ2bK2s60TJ4yyRhTXhGiwqdmxtDHlyU0dG25QYpirt
B4zIy2aSdEc=
=zJMk
-----END PGP SIGNATURE-----
