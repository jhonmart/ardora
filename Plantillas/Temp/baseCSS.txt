body{
	*FFA
	*BAC=none
	*BACPos=center
	*BACRep=repeat
	*BACCol=#FFFFFF 
	margin:5px 5px 5px 0;
	overflow:hidden;
}
#contenedor{
	height:auto;
	width:100%;
}
#menuPrincipal{
	height:28px;
	font-size:14px;
	font-weight:bold;
	display:inline;
	width:75%;
}
#menuPrincipal a {
	display: block;
	float: left;
	width: 166px;
	height: 25px;
	padding: 7px 1px 0 1px;
	background: url(images/boton.jpg) no-repeat center center;
	text-align: center;
	text-decoration: none;
	text-transform: uppercase;
	letter-spacing: .2em;
	font-size:12px;
	font-weight: bold;
	color:#314993;
}
#menuPrincipal a:hover{
	color:#FFFFFF;
}
#contenMenu{
	width: 100%;
	height:26px;
	clear:both;
	
}
#botones{
	width:21%;
	float:right;
	margin:0px;
	top:100px;
}

#cabecera{
	height:90px;
	background: url(images/ceras.jpg) no-repeat right bottom;
	background-color:#FFFFFF;
	margin-top:0px;
	padding-left:10px;
	border-top: none;
	border-left: none;
	border-right:none;
	border-bottom: solid 1px #000000;
	clear:both;
}
#cabecera h1{
	color:#000000;
	font-size:36px;
	left: 19px;
	width:75%;
	float:left;
}
#cabecera h2{
	margin:0px;
	color:#000000;
	float:right;
	display: inline;
	font-size:18px;
	width:20%;
}
#sombra{
	height:14px;
	width:100%;
	margin-bottom:5px;
	background: #fff url('images/border1.gif') repeat-x;
}
#menu{
clear:left;
	margin-left:10px;
	margin-top:10px;
	float:left;
	width:15%;
}
#menu h1{
	background: url(images/boton1.jpg) no-repeat left center;
	font-size:18px;
	border-bottom: solid 1px #B44325;
}
#menu h2{
	background: url(images/boton2.jpg) no-repeat left center;
	font-size:18px;
	border-bottom: solid 1px #B44325;
}
#menu a{
	font-size:14px;
	text-decoration:none;
	text-transform:uppercase;
	color:#000000;
	display:list-item;
	list-style:none;
}
#menu a:hover{
	
	text-decoration:none;
	text-transform:uppercase;
	background-color:#F6B969;
	color:#FFFFFF;
}
#areaContido{
	margin-top:10px;
	float:right;
	width:83%;
	border-left: dotted 1px #000000; text-align:justify;
}
#areaContido h1{
	margin:0 0 0 10px;
	font-size:18px
}
#areaContido h2{
	margin-left:10px;
	font-size:14px
}
#areaContido h3{
	margin:0 0 0 10px;
}
#areaContido a{
	font-size:16px;
	text-decoration:none;
	text-transform:uppercase;
	color:#000000;
}
#areaContido a:hover{
	text-decoration:none;
	text-transform:uppercase;
	background-color:#B44325;
	color:#FFFFFF;
}
#pie{
	width:100%;
	border-top: solid 1px #000000;
	clear:both;
	background-color:#9BC652;
	
}
#pie p{
	font-size:12px;
	margin:10px;
	color:#FFFFFF;
	
}